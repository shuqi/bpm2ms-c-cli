# BPM2MS #

This is an updated version of an older Java app I made a few years ago for calculating the milisecond values of a given tempo based on its beats per minute. Very useful for different audio applications such as dialing in pre-delay times and and setting up time based effects that don't have a MIDI-sync feature.